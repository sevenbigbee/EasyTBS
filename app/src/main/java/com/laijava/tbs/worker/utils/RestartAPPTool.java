package com.laijava.tbs.worker.utils;

import android.content.Context;
import android.content.Intent;

import com.laijava.tbs.worker.feature.HomeViewActivity;

public class RestartAPPTool {
    /**
     * 重启整个APP
     * @param context
     */
    public static void restartAPP(Context context){
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(HomeViewActivity.context.getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ActManager.getAppManager().finishAllActivity();

    }
}
