package com.laijava.tbs.worker.feature;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.laijava.tbs.worker.utils.ActManager;
import com.tencent.smtt.sdk.QbSdk;
import com.laijava.tbs.worker.R;

/**
 * Demo 基础 WebViewActivity，所有WebView能力Demo继承该 Activity 开发
 */
public class HomeViewActivity extends AppCompatActivity {
    private final String TAG = "HomeViewActivity";
    public static Context context;


    BroadcastReceiver receiver;
    LocalBroadcastManager localBroadcastManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (QbSdk.getTbsVersion(this) > 0) {
            Intent intent = new Intent(this, BaseWebViewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {


            ProgressBar progressBar = findViewById(R.id.progressBar);

            // 获取TextView的引用
            TextView textDescription = findViewById(R.id.textDescription);


            // 创建一个 BroadcastReceiver
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // 处理接收到的广播消息
                    int defaultValue = 0; // 默认值为0，你可以根据需要修改
                    int rate = intent.getIntExtra("rate",defaultValue);
                    if(rate != 0 ){
                        progressBar.setProgress(rate);
                        textDescription.setText("内核下载中..."+rate + "%");
                    }

                    String des = intent.getStringExtra("des");
                    if(null != des){
                        progressBar.setProgress(100);
                        textDescription.setText(des);
                    }

                    String state = intent.getStringExtra("state");
                    if("0".equals(state)){
                        textDescription.setText("完成安装请重启应用");
                        Button restartButton = findViewById(R.id.restartButton);
                        restartButton.setVisibility(View.VISIBLE); // 设置按钮可见
                        restartButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 关闭应用并重启
                                restartApp();
                            }
                        });
                    }
                }
            };

            // 获取 LocalBroadcastManager 实例
            localBroadcastManager = LocalBroadcastManager.getInstance(this);

            // 创建一个 IntentFilter，用于过滤特定的广播事件
            IntentFilter filter = new IntentFilter("custom-event-name");

            // 注册 BroadcastReceiver，指定 IntentFilter
            localBroadcastManager.registerReceiver(receiver, filter);
        }

        ActManager.getAppManager().addActivity(this);

    }


    @Override
    protected void onDestroy() {
        if (null != localBroadcastManager
        ){
            try {
                localBroadcastManager.unregisterReceiver(receiver);

            } catch (Exception e){
                Log.e(TAG,"error:" +e.getMessage());
            }

        }
        super.onDestroy();
    }

    public void restartApp() {

        Intent intent = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        if (intent != null) {
            finish(); // 关闭当前 Activity
            System.exit(0); // 终止当前进程
        }
    }
}
