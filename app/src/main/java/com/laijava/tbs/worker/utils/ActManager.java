package com.laijava.tbs.worker.utils;

import android.app.Activity;

import java.util.LinkedList;
import java.util.List;

public class ActManager {
    private static List<Activity> activityList = new LinkedList<>();
    private static ActManager instance;

    private ActManager() {
    }

    public static ActManager getAppManager() {
        if (instance == null) {
            instance = new ActManager();
        }
        return instance;
    }

    // 添加Activity到列表
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    // 移除Activity从列表
    public void removeActivity(Activity activity) {
        activityList.remove(activity);
    }

    // 关闭所有Activity
    public void finishAllActivity() {
        for (Activity activity : activityList) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
        activityList.clear();
    }
}
