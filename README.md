<h1 align="center">TBS SDK Demo</h1>

<p align="center">
<a href="https://x5.tencent.com/">TBS 简易工程</a>（Tencent Browsing Service, TBS）致力于提供优化移动端浏览体验的整套解决方案。
</p>

## 🌟 TBS 简易工程
在工作中，遇到了一些使用低版本Android设备的情况，这导致了一些挑战，因为这些设备自带的浏览器版本较低，不支持一些现代的JavaScript特性，尤其是ES6及更高级的功能。为了解决这个兼容性问题，我们采取了使用Android WebView来内嵌H5网页的策略，并使用了TBS（腾讯浏览服务）来整合。
TBS的整合解决了Android版本不一致和WebView兼容性的问题。该项目基于TBS官网的示例进行了简单的工程集成，使得项目启动后可以通过联网自动下载X5内核。一旦内核安装完成，只需重新启动应用程序，即可无缝地使用TBS的X5内核。
这种解决方案让我们能够在低版本Android设备上快速开发并运行H5网页，同时确保了兼容性和性能。这样一来，我们能够更好地满足用户的需求，无论他们使用的是哪种Android设备版本。